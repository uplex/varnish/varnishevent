This log file was created as follows:

* built varnish-cache documentation::

	cd $VARNISHSRC
	make html

* copied the resulting html to the docroot of a debian vanilla nginx::

	cp -r doc/sphinx/build/html/* /var/www/html

* started varnishd with the nginx as a backend::

	/tmp/sbin/varnishd -b 127.0.0.1:80 -a 127.0.0.1:8080 -n /tmp/v

* crawled the entire content with wget

	mkdir /tmp/scratch
	cd /tmp/scratch
	wget -r -l 99 http://localhost:8080

* dumped the entire VSM:

	/tmp/bin/varnishlog  -n /tmp/v -d -g raw -w varnish-post-7.3.vsl

Version used:

$ /tmp/sbin/varnishd -V
varnishd (varnish-trunk revision a84b3c9416890a3f32e25ce4f6d6c87e40b10c79)
