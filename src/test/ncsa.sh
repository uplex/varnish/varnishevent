#!/bin/bash

function diffem() {
    local what="${1}";shift;

    if ! diff "${@}" ; then
	echo "ERROR: ${what} differ"
	echo "       see ${@}"
	exit 1
    fi

    rm -f "${@}"
}

# Outputs of varnishevent and varnishncsa for client formats are
# identical, except that varnishevent emits empty strings for data
# from headers that are empty, and varnishnca emits a '-'.

echo
echo "TEST: $0"
echo "... testing equivalence of output with varnishncsa"

# automake skip
SKIP=77

TMP=${TMPDIR:-/tmp}
EVENT="../varnishevent"
NCSA=$( which varnishncsa )

if [ -x "$NSCA" ]; then
    echo "varnishncsa not found or not executable ($NCSA), skipping"
    exit $SKIP
fi

EVENT_LOG=$TMP/event.log
NCSA_LOG=$TMP/ncsa.log
INPUT=varnish-post-7.3.vsl

echo "... default format"
$EVENT -r $INPUT | sed 's/-//g' > $EVENT_LOG
$NCSA -r $INPUT | sed 's/-//g' > $NCSA_LOG

diffem "outputs of no-arg varnishevent and varnishncsa" \
       $EVENT_LOG $NCSA_LOG

# untested:
# %D formatter : varnishncsa has floating point errors, varnishevent is accurate
# %{VSL:Debug}x: unsupported in varnishncsa
FORMAT='%b %H %h %I %{Host}i %{Connection}i %{User-Agent}i %{X-Forwarded-For}i %{Accept-Ranges}o %{Age}o %{Connection}o %{Content-Encoding}o %{Content-Length}o %{Content-Type}o %{Date}o %{Last-Modified}o %{Server}o %{Transfer-Encoding}o %{Via}o %{X-Varnish}o %l %m %O %q %r %s %t %{%F-%T}t %U %u %{Varnish:time_firstbyte}x %{Varnish:hitmiss}x %{Varnish:handling}x %{VSL:Begin}x %{VSL:End}x %{VSL:Gzip}x %{VSL:Hit}x %{VSL:Length}x %{VSL:Link}x %{VSL:ReqAcct}x %{VSL:ReqStart}x %{VSL:RespProtocol}x %{VSL:ReqMethod}x %{VSL:ReqURL}x %{VSL:ReqProtocol}x %{VSL:RespReason}x %{VSL:RespStatus}x %{VSL:Timestamp}x %{Varnish:vxid}x %{Varnish:side}x'

echo "... custom -F format"
$EVENT -r $INPUT -F "$FORMAT" | sed 's/-//g' > $EVENT_LOG
$NCSA -r $INPUT -F "$FORMAT" | sed 's/-//g' > $NCSA_LOG

diffem "outputs of varnishevent/varnishncsa -F" \
       $EVENT_LOG $NCSA_LOG

FORMAT='%b %H %h %I %{Host}i %{User-Agent}i %{X-Forwarded-For}i %{Accept-Ranges}o %{Connection}o %{Content-Encoding}o %{Content-Length}o %{Content-Type}o %{Date}o %{Last-Modified}o %{ETag}o %{Server}o %l %m %O %q %r %s %t %{%F-%T}t %U %u %{Varnish:time_firstbyte}x %{VSL:Begin}x %{VSL:End}x %{VSL:Link}x %{VSL:Timestamp}x %{Varnish:vxid}x %{Varnish:side}x'

# Skip the first line of varnishevent output ("Reading config file ...")
# Remove \" escaping from varnishncsa output (done for ETag)
echo "... client and backend logging"
$EVENT -r $INPUT -f backend_client.conf | tail -n +2 | sed 's/-//g' > $EVENT_LOG
$NCSA -r $INPUT -F "$FORMAT" -b -c | sed 's/-//g; s/\\"/"/g' > $NCSA_LOG

diffem "varnishevent/varnishncsa backend and client logs" \
       $EVENT_LOG $NCSA_LOG

FORMAT_EVENT='%{VSL:Timestamp[1]}x'
FORMAT_NCSA='%{VSL:Timestamp[2]}x'

echo "... VSL formatter"
$EVENT -r $INPUT -F "$FORMAT_EVENT" > $EVENT_LOG
$NCSA -r $INPUT -F "$FORMAT_NCSA"  > $NCSA_LOG

diffem "outputs of varnishevent/varnishncsa VSL formatter" \
       $EVENT_LOG $NCSA_LOG

FORMAT_EVENT='%{tag:Timestamp[1]}x'
FORMAT_NCSA='%{VSL:Timestamp[2]}x'

echo "... compatibility of tag and VSL formatter"
$EVENT -r $INPUT -F "$FORMAT_EVENT" > $EVENT_LOG
$NCSA -r $INPUT -F "$FORMAT_NCSA" > $NCSA_LOG

diffem "tag and VSL formatters for varnishevent and varnishncsa" \
       $EVENT_LOG $NCSA_LOG

FORMAT_EVENT='%{vxid}x'
FORMAT_NCSA='%{Varnish:vxid}x'

echo "... compatibility of the vxid and Varnish:vxid formatters"
$EVENT -r $INPUT -F "$FORMAT_EVENT" > $EVENT_LOG
$NCSA -r $INPUT -F "$FORMAT_NCSA" > $NCSA_LOG

diffem "vxid and Varnish:vxid formatters for varnishevent and varnishncsa differ" \
       $EVENT_LOG $NCSA_LOG

exit 0
