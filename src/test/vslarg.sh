#!/bin/bash

echo
echo "TEST: $0"
echo "... testing standard VSL args"

IN=varnish-post-7.3.vsl
CONF=varnishevent.conf
LOG=/dev/null
TRC=$(basename $0).trc

rm -rf ${TRC}.*

# Ensure that the local time date formatters produce the same output
# wherever the test is run.
export TZ=UTC

echo "... no VSL args"
../varnishevent -r ${IN} -f ${CONF} -l ${LOG} > ${TRC}.no
echo "... -g vxid"
../varnishevent -g vxid -r ${IN} -f ${CONF} -l ${LOG} > ${TRC}.vxid
echo "... -g request"
../varnishevent -g request -r ${IN} -f ${CONF} -l ${LOG} > ${TRC}.request
echo "... -g raw"
# Timestamps for raw grouping are always the time at which the tx was read,
# even for binary file reads. So we check against the last four columns.
# The query restricts output to Begin records; the previous invocation
# rendered every record with just the VXIDs.
../varnishevent -g raw -r ${IN} -f raw.conf -l ${LOG} -q 'Begin' |
    cut -d' ' -f4- > ${TRC}.raw
# Cannot mix raw grouping with client and/or backend formats
../varnishevent -g raw -f ${CONF}  -l ${LOG}

if [ "$?" != "1" ]; then
    echo "ERROR: -g raw with client/backend formats did not exit with failure as expected"
    exit 1
fi

echo '... -g session'
../varnishevent -g session -l ${LOG}

if [ "$?" != "1" ]; then
    echo "ERROR: -g session did not exit with failure as expected"
    exit 1
fi

echo "... -q query"
../varnishevent -q 'ReqURL ~ "_static"' -r ${IN} -l ${LOG} > ${TRC}.query

../varnishevent -q 'ReqURL ~' -l ${LOG}

if [ "$?" != "1" ]; then
    echo "ERROR: -q query with illegal VSL query did not exit with failure as expected"
    exit 1
fi

echo "... -C"
../varnishevent -C -q 'ReqURL ~ "_STATIC"' -r ${IN} -l ${LOG} > ${TRC}.dash-C

set -eu

tmpf=/tmp/$(basename $0).cmp.$$
cksum ${TRC}.* >${tmpf}
if diff $(basename $0 .sh).cksum ${tmpf} ; then
    rm -rf ${TRC}.*
else
    echo "ERROR: checksum diff"
    exit 1
fi

rm -f ${tmpf}
